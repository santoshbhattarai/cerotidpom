package tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import pages.CerotidPage;
import pages.CerotidPageObjectsAndMethods;

public class CerotidPageObjectsAndMethodsTest {
	
	public static WebDriver driver = null;
	
	public static void main(String[] args) {
		
		// Invoke the Browser
		invokeBrowser();
		
		// Fill the Form
		fillForm();
	}

	public static void fillForm() {
		CerotidPageObjectsAndMethods cerotidPageObj = new CerotidPageObjectsAndMethods(driver);
		cerotidPageObj.selectCourse();
		
		CerotidPageObjectsAndMethods selectSession = new CerotidPageObjectsAndMethods(driver);
		selectSession.selectSession();
		
		CerotidPageObjectsAndMethods fullName = new CerotidPageObjectsAndMethods(driver);
		fullName.fullName();
		
		
		CerotidPageObjectsAndMethods fieldAddress = new CerotidPageObjectsAndMethods(driver);
		fieldAddress.fieldAddress();
		
		

		CerotidPageObjectsAndMethods fieldCity = new CerotidPageObjectsAndMethods(driver);
		fieldCity.fieldCity();
		
		CerotidPageObjectsAndMethods selectState = new CerotidPageObjectsAndMethods(driver);
		selectState.selectState();

		
		CerotidPageObjectsAndMethods fieldZipcode = new CerotidPageObjectsAndMethods(driver);
		fieldZipcode.fieldZipcode();
		
		CerotidPageObjectsAndMethods fieldEmail = new CerotidPageObjectsAndMethods(driver);
		fieldEmail.fieldEmail();
		
		CerotidPageObjectsAndMethods fieldPhone = new CerotidPageObjectsAndMethods(driver);
		fieldPhone.fieldPhone();
		
		CerotidPageObjectsAndMethods selectVisastatus = new CerotidPageObjectsAndMethods(driver);
		selectVisastatus.selectVisastatus();
		
		CerotidPageObjectsAndMethods fieldMediaSourse = new CerotidPageObjectsAndMethods(driver);
		fieldMediaSourse.fieldMediaSourse();
		
		CerotidPageObjectsAndMethods fieldClickNOBTn = new CerotidPageObjectsAndMethods(driver);
		fieldClickNOBTn.fieldClickNOBTn();
		
		CerotidPageObjectsAndMethods fieldEducation = new CerotidPageObjectsAndMethods(driver);
		fieldEducation.fieldEducation();
		
		
		
		
		
		
	}

	public static void invokeBrowser() {
		//a: set the system path
				System.setProperty("webdriver.chrome.driver",".\\libs\\chromedriver.exe");
				
				//b. Chromedriver object
				driver = new ChromeDriver();
				
				//c. Navigate to the website
				driver.get("http://www.cerotid.com/");
				
				//d. Validate the Title
				if(driver.getTitle().contains("Cerotid")) {
					System.out.println(driver.getTitle() + "..........was lunched");
				}
				
				// Maximize the Browser
				driver.manage().window().maximize();
				
				
				// Add wait for the page to load
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				
		
	}

}
