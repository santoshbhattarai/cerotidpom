package tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import pages.FaceBookPage;

public class FaceBookPageTest {
	
	private static WebDriver driver = null;

	public static void main(String[] args)   {
		// Invoke the Browser
		invokeBrowser();
		
		// Fill the form
		fillForm();
		
		// Terminate Browser
		
	}

	public static void fillForm() {
		
		// Utilizing the FaceBookPage object 
		
		// entering fullname
		FaceBookPage.enterFirstName(driver).sendKeys("Santosh");
		
		// entering lastName
		FaceBookPage.enterLastName(driver).sendKeys("Bhattarai");
		
		// entring phonenumber
		FaceBookPage.enterPhoneNumber(driver).sendKeys("1234567890");
		
		// entering newpassword
		FaceBookPage.enterNewPassword(driver).sendKeys("Bhattarai@000");
		
		//select Birthmonth
		Select selectBirthmonth = new Select(FaceBookPage.selectBirthmonth(driver));
		selectBirthmonth.selectByVisibleText("Aug");
		
		// selecting birthday
		Select selectBirthday = new Select(FaceBookPage.selectBirthday(driver));
		selectBirthday.selectByValue("7");
		
		// selecting Birthyear
		Select selectBirthyear = new Select(FaceBookPage.selectBirthyear(driver));
		selectBirthyear.selectByValue("1993");
		
		// clicking on male button
		FaceBookPage.clickONmaleBtn(driver).click();
		
		
		
		
		
	}

	public static void invokeBrowser()   {
		
		//1.Set the system Path
		System.setProperty("webdriver.chrome.driver",".\\libs\\chromedriver.exe");
		
		//2.Create Chromedriver object
		driver = new ChromeDriver();
		
		//3.Navigate to the Browser
		driver.get("https://www.facebook.com/");
		
		
		//4.Validate the title
		if(driver.getTitle().contains("Facebook")) {
			System.out.println(driver.getTitle() + "..........was lunched");
			
		}
		//5.Maximize the Browser
		driver.manage().window().maximize();
		
		

		// Add wait for the page to load
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
	}

	
		
	}


