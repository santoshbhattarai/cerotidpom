package tests;

import java.util.concurrent.TimeUnit;

//import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import pages.CerotidPage;

public class CerotidPageTest {

	private static WebDriver driver = null;
	
	public static void main(String[] args) {
		
		//1. Invoke Browser
		invokeBrowser();
		
		//2. Fill form
		fillForm();
		
		//3. Terminate Browser
	}

	public static void fillForm() {
		//Utilizing the CerotidPage objects/Elements to select course
		
		
		//1. Choose Course
		Select chooseCourse = new Select(CerotidPage.selectCourse(driver));
		chooseCourse.selectByVisibleText("QA Automation");
		

		
		//2. Select Session
		Select selectSession = new Select(CerotidPage.selectSession(driver));
		selectSession.selectByVisibleText("Upcoming Session");
		
		
		//3. Full Name
		CerotidPage.fullName(driver).sendKeys("Test");
		
		//4. Field Address
		CerotidPage.fieldAddress(driver).sendKeys("123 Medical Dr");
		
		//5. Field city
		CerotidPage.fieldCity(driver).sendKeys("San Antonio");
		
		//6. Select state
		Select selectState = new Select(CerotidPage.selectState(driver));
		selectState.selectByVisibleText("TX");
		
		//7.Field Zip Code
		CerotidPage.fieldZipcode(driver).sendKeys("78229");
		
		//8. Field Email
		CerotidPage.fieldEmail(driver).sendKeys("test2050@gmail.com");
		
		//9. Field Phone
		CerotidPage.fieldPhone(driver).sendKeys("1234567890");
		
		//10. Field VisaStatus
		CerotidPage.selectVisastatus(driver).sendKeys("GreenCard");
		
		//11. Field Mediasourse
		CerotidPage.fieldMediaSourse(driver).sendKeys("Friends/Family");
		
		//12. Click to No btn
		CerotidPage.fieldClickNOBTn(driver).click();
		
		//13. Field Education BackGround
		CerotidPage.fieldEducation(driver).sendKeys("QA Automation");
		
		
	}

	public static void invokeBrowser() {
		//a: set the system path
		System.setProperty("webdriver.chrome.driver",".\\libs\\chromedriver.exe");
		
		//b. Chromedriver object
		driver = new ChromeDriver();
		
		//c. Navigate to the website
		driver.get("http://www.cerotid.com/");
		
		//d. Validate the Title
		if(driver.getTitle().contains("Cerotid")) {
			System.out.println(driver.getTitle() + "..........was lunched");
		}
		
		// Maximize the Browser
		driver.manage().window().maximize();
		
		
		// Add wait for the page to load
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		
	}
}
