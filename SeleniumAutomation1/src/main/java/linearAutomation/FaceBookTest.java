package linearAutomation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class FaceBookTest {

	public static void main(String[] args) {
		//1. Set the system path
		System.setProperty("webdriver.chrome.driver" , ".\\libs\\chromedriver.exe");
		
		
		//2. Creating New ChromeDriver
		WebDriver driver = new ChromeDriver();
		
		//3. Invoke the Chromedriver object
		driver.get("https://www.facebook.com");
		
		
		//4. Maximize the Browser
		driver.manage().window().maximize();
		
		
		//5. Clicking on the Gender button
		WebElement ableToRelocateFemaleBTn = driver.findElement(By.xpath("//input[@id='u_0_6']"));
		ableToRelocateFemaleBTn.click();
		//input[@value='u_0_6']
	}

}
