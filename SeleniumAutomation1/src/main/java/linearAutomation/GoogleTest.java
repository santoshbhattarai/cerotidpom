package linearAutomation;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class GoogleTest {
	public static void main(String[] args) {
		
		//1: Setting the system path- providing location of the chromedriver
	System.setProperty("webdriver.chrome.driver",".\\libs\\chromedriver.exe");
	
	//2: Creating new chromedriver object
	WebDriver driver = new ChromeDriver();
	
	//3: Invoke the chromedriver object
	driver.get("https://www.Google.com");
	
	//4: Maximize the browser
	driver.manage().window().maximize();
	
	//5: Validate the title is "Google"
	if(driver.getTitle().contains("Google")) {
		System.out.println("Passed- Navigated to valid page.");
	}else {
		System.out.println("Failed- invalid page.");
	}
	
	// Perfrom some action on the UI/Webpage
	// Test case: Navigate to Google.com and search for what is selenium
	
	//1: Find the Elements needed to interact with the UI for our Test case
	//note: Two ways to interact with elements on  a page
	
	//a: Absolute xpath = It contains the complete path from the root element to the desire element
	
	//b: Relative xpath = This is more like starting simply by referencing the element you want and go from the particular current node
	
	//xpath = search field //input[@name='q']
	// // = current node
	// input = tagname
	//@ = selects attribute
	//q = value of the attribute
	
	WebElement txtBoxSearch = driver.findElement(By.xpath("//input[@name='q']"));
	txtBoxSearch.sendKeys("What is selenium");
	
	//pressing enter
	txtBoxSearch.sendKeys(Keys.RETURN);
	
	//validate after search
	if(driver.getTitle().contains("What is selenium - Google Search")) {
		System.out.println("Passed- Navigated to valid page");
	}else {
		System.out.println("Failed- invalid page");
	}
	
	
	
	//6: Close the browser
	driver.close();
	
	//Terminate the browser
	driver.quit();
	
		
	}
	 

}
