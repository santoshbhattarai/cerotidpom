package linearAutomation;

import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CerititWebsiteLiner {

	public static void main(String[] args) {
		// step 1: Invoke Browser Navigate to the cerotid page
		// a. set the system Path
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		
		//b. create a chromedriver
		WebDriver driver = new ChromeDriver();
		
		//c. Navigate to cerotid page
		//c1. Get url
		//driver.get("http://www.cerotid.com");
		
		//c2. Navigate to url
		driver.navigate().to("http://www.cerotid.com");
		
		//c3. Maximize the browser
		driver.manage().window().maximize();
		
		
		
		// Step 2: Fill form in cerotid page
		//a. Created a webElement object 
		WebElement selectCourse = driver.findElement(By.xpath("//select[@id='classType']"));
		
		//b. Create a select obj and pass the element we want to select
		Select chooseCourse = new Select(selectCourse);
		
		//c. creating a string variable with course name
		String courseName = "QA Automation";
		
		//d. Selecting the course by visible text
		chooseCourse.selectByVisibleText(courseName);
		
		
		//Create a session type webelement object
		WebElement sessionType = driver.findElement(By.xpath("//select[@id='sessionType']"));
		Select chooseSession = new Select(sessionType); 
		String session = "Upcoming Session";
		chooseSession.selectByVisibleText(session);
		
		//Creating a First Name webelement
		WebElement fullName = driver.findElement(By.xpath("//input[@placeholder='Full Name *']"));
		fullName.sendKeys("Test Tester");
		
		//Creating a address webelement
		WebElement addressFeild = driver.findElement(By.xpath("//input[@placeholder='Address *']"));
		addressFeild.sendKeys("75045 tester dr, Irving Tx");
		
		//creating a city webelement
		WebElement cityFeild = driver.findElement(By.xpath("//input[@placeholder='Address *']"));
		cityFeild.sendKeys("San Antonio");
		
		
		//Find all element inside the state dropdown and choose the state
		
		// Locating the element and storing them in a list
		List<WebElement> listofStates = driver.findElements(By.xpath("//select[@id='state']/option"));
		
		// looping through the list of webelements
		Iterator<WebElement> elementIterrator = listofStates.iterator();
		
		
		//Crating a state webelement
		
		
		// clicking on the No Radio button
		WebElement ableToRelocateNOBTn = driver.findElement(By.xpath("//input[@id='N0']"));
		ableToRelocateNOBTn.click();
		
		// Step 3: Validate Success Message
		

	}

}
