package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

// Santosh Bhattarai: This class will hold web elements from cerotid and help us understand page object model
// This class will return elements to class cerotidPageTest

public class CerotidPage {

	 // Class level Variable to store web element value
		private static WebElement element = null;
		
	// Course element
		public static WebElement selectCourse(WebDriver driver) {
		//FInd the Element and return the Value
		   element = driver.findElement(By.xpath("//select[@id='classType']"));
		   return element;
			
		}
		
	//Session date element
		public static WebElement selectSession(WebDriver driver) {
		  element = driver.findElement(By.xpath("//select[@id='sessionType']"));
		  return element;
		}
		
	//Full Name element
		public static WebElement fullName(WebDriver driver) {
		  element = driver.findElement(By.xpath("//input[@placeholder='Full Name *']"));
		  return element;
		 
	
		}
		
	//Field address element
		public static WebElement fieldAddress(WebDriver driver) {
			 element = driver.findElement(By.xpath("//input[@id='address']"));
			 return element;
			   
		 
		}
		
	//Field City element
		public static WebElement fieldCity(WebDriver driver) {
			element = driver.findElement(By.xpath("//input[@id='city']"));
			return element;
					   
				 
		}
	//Field State element
		public static WebElement selectState(WebDriver driver) {
			element = driver.findElement(By.xpath("//select[@id='state']"));
			return element;
							   
						 
				}
					
	//Field Zip Code element
		public static WebElement fieldZipcode(WebDriver driver) {
			element = driver.findElement(By.xpath("//input[@placeholder='Zip Code *']"));
			return element;
									   
								 
				}
						
		
		
	//Field Email element
		public static WebElement fieldEmail(WebDriver driver) {
			element = driver.findElement(By.xpath("//input[@placeholder='Email *']"));
			return element;
											   
										 
				}
					
		
	//Field Phone element
		public static WebElement fieldPhone(WebDriver driver) {
			element = driver.findElement(By.xpath("//input[@placeholder='Phone *']"));
			return element;
													   
												 
				}
	//Select Visa Status element
		public static WebElement selectVisastatus(WebDriver driver) {
			element = driver.findElement(By.xpath("//select[@id='visaStatus']"));
			return element;
															   
														 
				}
	//Field  Mediasourse element
		public static WebElement fieldMediaSourse(WebDriver driver) {
			element = driver.findElement(By.xpath("//select[@id='mediaSource']"));
			return element;
																	   
																 
				}
	//Field  ClickNoBTn element
		public static WebElement fieldClickNOBTn(WebDriver driver) {
			element = driver.findElement(By.xpath("//input[@value='N0']"));
			return element;
																			   
																		 
				}
										
	//Field  Education element
		public static WebElement fieldEducation(WebDriver driver) {
			element = driver.findElement(By.xpath("//textarea[@placeholder='Education Background *']"));
			return element;
																					   
																				 
				}
																	
		
		
		
}

		

		

