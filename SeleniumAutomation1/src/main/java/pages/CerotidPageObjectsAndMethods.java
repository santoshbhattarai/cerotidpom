package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CerotidPageObjectsAndMethods {
	
	
    // Creating a empty webdriver
	WebDriver driver = null;
	
	// Locating Elements Using By object
	By selectCourse = By.xpath("//select[@id='classType']");
	By selectSession = By.xpath("//select[@id='sessionType']");
	By fullName = By.xpath("//input[@placeholder='Full Name *']");
	By fieldAddress = By.xpath("//input[@id='address']");
	By fieldCity = By.xpath("//input[@id='city']");
	By selectState = By.xpath("//select[@id='state']");
	By fieldZipcode = By.xpath("//input[@placeholder='Zip Code *']");
	By fieldEmail = By.xpath("//input[@placeholder='Email *']");
	By fieldPhone = By.xpath("//input[@placeholder='Phone *']");
	By selectVisastatus = By.xpath("//select[@id='visaStatus']");
	By fieldMediaSourse = By.xpath("//select[@id='mediaSource']");
	By fieldClickNOBTn = By.xpath("//input[@value='N0']");
	By fieldEducation = By.xpath("//textarea[@placeholder='Education Background *']");
	
	
	
	
	public CerotidPageObjectsAndMethods(WebDriver driver) {
		
		this.driver = driver;
	}
	
   
	public void selectCourse() {
	   
		driver.findElement(selectCourse).sendKeys("QA Automation");
		
   }
	
	public void selectSession() {
		
		driver.findElement(selectSession).sendKeys("Upcoming Session");
	}
	
	public void fullName() {
		
		driver.findElement(fullName).sendKeys("Test case");
	}
	
   public void fieldAddress() {
		
		driver.findElement(fieldAddress).sendKeys("123 new address");
	}
	
   public void fieldCity() {
		
		driver.findElement(fieldCity).sendKeys("San Antonio");
	}
	
   
   public void selectState() {
		
		driver.findElement(selectState).sendKeys("TX");
	}
	
   
   public void fieldZipcode() {
		
	 if (isElementPresent(fieldZipcode)) {
		 System.out.println("Element Zip Code Found");
		// driver.findElement(fieldZipcode).sendKeys(zip);
	  
		driver.findElement(fieldZipcode).sendKeys("78090");
	}else {
		System.out.println("Element not Located");
	}
	}
	
   
   public void fieldEmail() {
		
		driver.findElement(fieldEmail).sendKeys("test@gmail.com");
	}
	
   
   public void fieldPhone() {
		
		driver.findElement(fieldPhone).sendKeys("1234567890");
	}
   
   public void selectVisastatus() {
		
		driver.findElement(selectVisastatus).sendKeys("GreenCard");
	}
  
   public void fieldMediaSourse() {
		
		driver.findElement(fieldMediaSourse).sendKeys("Frainds/Family");
	}
 
   public void fieldClickNOBTn() {
		
		driver.findElement(fieldClickNOBTn).click();
	}

   public void fieldEducation() {
		
		driver.findElement(fieldEducation).sendKeys("High Schoole Diploma");
	}
	
   // Frame work Method (check if a element is present or not)
   
   public boolean isElementPresent(By by) {
	   
	   boolean check = false;
	   try {
		   driver.findElement(by);
		   check = true;
	   }catch(Exception e) {
		   
		   e.printStackTrace();
		   e.getMessage();
		   e.getCause();
		   
		   check = false;
		   
		   
	   }
	   return check;
	   
   }
   
   
   
   
   
   
}


