package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
// we are going to test facebook page by page object model(POM) method


public class FaceBookPage {

	// Class level Variable to store web element value
	private static WebElement element = null;
	
	
	// First Name element
	public static WebElement enterFirstName(WebDriver driver) {
		//a. find the element and return the value
		element = driver.findElement(By.xpath("//input[@name='firstname']"));
		return element;
		
		}
	// Last Name element
	public static WebElement enterLastName(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@name='lastname']"));
		return element;
		
	}
	// MObile number or Email
	public static WebElement enterPhoneNumber(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@name='reg_email__']"));
		return element;
		
	}
	// New password element
	public static WebElement enterNewPassword(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@id='password_step_input']"));
		return element;
	}
	// Birthday month element
	public static WebElement selectBirthmonth(WebDriver driver) {
		element = driver.findElement(By.xpath("//select[@id='month']"));
		return element;
		
	}
    // Birthday day element
	public static WebElement selectBirthday(WebDriver driver) {
		element = driver.findElement(By.xpath("//select[@id='day']"));
		return element;
		
	}
	// Birthday year element
	public static WebElement selectBirthyear(WebDriver driver) {
		element = driver.findElement(By.xpath("//select[@id='year']"));
		return element;
		
	}
	// Click on male element
	public static WebElement clickONmaleBtn(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@id='u_0_7']"));
		return element;
	}
	
	
	
	
	
	
}
