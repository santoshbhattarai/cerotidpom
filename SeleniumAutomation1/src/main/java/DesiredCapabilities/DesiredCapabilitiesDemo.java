package DesiredCapabilities;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
//import org.openqa.selenium.remote.DesiredCapabilities;

public class DesiredCapabilitiesDemo {
	//1. How to invoke browser in incognito mode
	//2. How to invoke browser in headless mode
	//3. How to remove adds in a browser
	
public static void main(String[] args) {
	
	//a. Set the SystemPath
	System.setProperty("webdriver.chrome.driver",".\\libs\\chromedriver.exe");
	
	
	// OR
	//System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir") + "/Drivers/chromedriver.exe");
	
	//Invoke browser in incognito mode
	ChromeOptions options = new ChromeOptions();
	options.addArguments("--incognito");
	
	
	WebDriver driver = new ChromeDriver(options);
	
	// Maximize the Browser
		driver.manage().window().maximize();
		
	driver.get("https://www.google.com/");
	
	System.out.println(driver.getTitle());
	
	//2. Invoke Browser in Headless mode
	
	/*ChromeOptions options = new ChromeOptions();
	options.addArguments("--headless");
	
	WebDriver driver = new ChromeDriver(options);
	driver.get("https://www.google.com/");
	System.out.println(driver.getTitle());*/
	
	//3. Ad Blocker
	/**ChromeOptions options = new ChromeOptions();
	options.addExtensions(new File(".\\libs\\extension_4_15_0_0 (1).crx"));
	
	WebDriver driver = new ChromeDriver(options);
	driver.get("https://www.guru99.com/smoke-testing.html#7");
	
	// Maximize the Browser
	driver.manage().window().maximize();**/
	
	
}
	
	
	
	
	
	
	
}
