package frontierAirlines;

//import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class FrontierAirlines {

	static WebDriver driver;
	
public static void main(String[] args) throws Throwable {
	
	//1: Invoke Browser
	invokeBrowser();
	
	//2. Fill the Form
	fillForm();
	//3. Validate the Page
	
	//4. Terminate the Browser
	
}

public static void fillForm() throws Throwable {
	// Close cookies Option
	WebElement cookieXBtn = driver.findElement(By.xpath("(//button[@aria-label='Close'])[3]"));
	cookieXBtn.click();
	
	// Form Element
	WebElement fromCity = driver.findElement(By.xpath("//input[@name='kendoDepartFrom_input']"));
	fromCity.sendKeys("DFW");
	// Give time to load element
	Thread.sleep(3000);
	
	fromCity.sendKeys(Keys.TAB);
	//fromCity.sendKeys(Keys.TAB);
	// Add wait for the page to load
	//driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
	
	
	//Give time
	//Thread.sleep(5000);
	
	
	// To Element
	WebElement toCity = driver.findElement(By.xpath("//input[@name='kendoArrivalTo_input']"));
	//input[@id='kendoArrivalTo']
	toCity.sendKeys("LAX");
	Thread.sleep(5000);
	toCity.sendKeys(Keys.RETURN);
	
	
	// Finding Element to click on Calendar icon
	WebElement calenderIcon = driver.findElement(By.xpath("//img[@id='departureDateIcon']"));
	calenderIcon.click();
	
	// Click on Departure date
	WebElement departureDate = driver.findElement(By.xpath("//a[@id='7-15-2020']"));
	departureDate.click();
	
	//Click on Return Date
	WebElement returnDate = driver.findElement(By.xpath("//a[@id='7-22-2020']"));
	returnDate.click();
	returnDate.sendKeys(Keys.RETURN);
	
	// Store the current window
	String firstWindows = driver.getWindowHandle();
	
	// Click close when new window opens
	for(String windows : driver.getWindowHandles()) {
		driver.switchTo().window(windows);
		
	}
	
	driver.close();
	driver.switchTo().window(firstWindows);
	
	//returnDate.click();

	//Select travelers
	
	
	
	
	
	
}

public static void invokeBrowser() throws Throwable {
	
    //a: set the system path
	System.setProperty("webdriver.chrome.driver",".\\libs\\chromedriver.exe");
	
	//b. Chromedriver object
	driver = new ChromeDriver();
	
	//c. Navigate to the website
	driver.get("https://www.flyfrontier.com/");
	
	//d. Validate the Title
	if(driver.getTitle().contains("Low Fares Done Right | Frontier Airlines")) {
		System.out.println(driver.getTitle() + "..........was lunched");
	}
	
	// Maximize the Browser
	driver.manage().window().maximize();
	
	// Add wait for the page to load
	Thread.sleep(5000);
	
}


}
