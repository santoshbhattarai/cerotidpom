package santoshExcersisePlatform;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class FindwidthofText {

	public static void main(String[] args) {
		
		//1. Set the system path
		
		System.setProperty("webdriver.chrome.driver",".//libs//chromedriver.exe");
		
		//2. Create the chrome object
		WebDriver driver = new ChromeDriver();
		
		//3. Invoke the Browser or Navigate to Browser
		driver.get("https://www.facebook.com/");
		
		//4. Maximize the browser
		driver.manage().window().maximize();
		
		//5. Find the element of email Box
		int width = driver.findElement(By.xpath("//input[@id='email']")).getSize().getWidth();
		
		System.out.println(width);
		
		if(width<9) {
			driver.findElement(By.xpath("//input[@id='email']")).sendKeys("0987543255");
			
		}else {
			System.out.println("Please Enter the Correct Phone Number");
		}
		// www.alltestingstuff.com
	}

}
