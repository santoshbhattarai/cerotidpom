package santoshExcersisePlatform;

import org.openqa.selenium.By;
//import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class UseIsDisplayed {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://www.google.com/");
		
		//boolean Test = driver.findElement(By.xpath("//input[@data-ved='0ahUKEwjGybPIjqDqAhXVs54KHclSDL4Q4dUDCA0']")).isDisplayed();
		
		//System.out.println(Test);
		if(driver.findElement(By.xpath("//input[@data-ved='0ahUKEwjGybPIjqDqAhXVs54KHclSDL4Q4dUDCA0']")).isDisplayed()) {
			System.out.println("I am on Correct Page");
		}else {
			System.out.println("Wrong page");
		}

	}
	//System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
	
}
