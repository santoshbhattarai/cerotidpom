package santoshExcersisePlatform;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ClickForwardBackwarkBTN {
public static void main(String[] args) throws InterruptedException {
	// Set the system path
	System.setProperty("webdriver.chrome.driver",".\\libs\\chromedriver.exe");
	
	// Create the Chromedriver Obj
	WebDriver driver = new ChromeDriver();
	
	// Navigate to the Browser
	driver.get("https://www.google.com/");
	
	// Navigate to the Next Browser
	driver.get("https://www.google.com/search?sxsrf=ALeKk02YFhTwvr-beljs95F8BmeaKIQHww%3A1593107562760&source=hp&ei=auT0XpqXLKyy0PEPmueewA4&q=what+is+facebook&oq=what+is+Face&gs_lcp=CgZwc3ktYWIQARgBMgUIABCxAzIFCAAQsQMyAggAMgIIADICCAAyAggAMgIIADICCAAyAggAMgIIADoHCCMQ6gIQJzoECCMQJzoECAAQQzoFCAAQkQI6BQgAEIMBOgQIABAKUMYoWOlPYLFhaAFwAHgBgAGgBIgBmBSSAQkwLjkuMi41LTGYAQCgAQGqAQdnd3Mtd2l6sAEK&sclient=psy-ab");
	
	// Give time to page
	Thread.sleep(5000);
	
	// Create Back button
	driver.navigate().back();
	
	// Give a time
	Thread.sleep(5000);
	
	// Create Forward Button
	driver.navigate().forward();
	
	// Close the browser
	driver.close();
	
	
}
}
