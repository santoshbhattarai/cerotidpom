package santoshExcersisePlatform;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ValidatingPage {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver",".\\libs\\chromedriver.exe");
		
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://flights.aeromexico.com/en-us/flights-from-united-kingdom");

	String ActualTitle = "Aeromexico flights from United Kingdom | Aeromexico";
	String PageTitle = driver.getTitle();
	
	if(ActualTitle.equals(PageTitle)) {
		
		System.out.println("I am on correct page");
		
	}else {
		
		System.out.println("I am on wrong page");
	}
	
	
	
	
	}

}
