package santoshExcersisePlatform;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class UseContainstoCreatXPATH {

	public static void main(String[] args) throws Throwable {
		//1. Set the system path
		
			System.setProperty("webdriver.chrome.driver",".//libs//chromedriver.exe");
				
			//2. Create the chrome object
			WebDriver driver = new ChromeDriver();
				
			//3. Invoke the Browser or Navigate to Browser
			driver.get("https://www.facebook.com/");
				
			//4. Maximize the browser
			driver.manage().window().maximize();
			
			//5. Give time to click on create page
			Thread.sleep(3000);
			
			
			//6.Finding the Contains element for create a account
			driver.findElement(By.xpath("//a[contains(text(),'Create a Page')]")).click();
			
			//&.Give Time to Click on Back Button
			Thread.sleep(5000);
			
			//8. Click on the backward Button
			driver.navigate().back();
		
				

	}

}
