package santoshExcersisePlatform;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class GetandReadTextBox {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver",".\\libs\\chromedriver.exe");
		
		WebDriver driver = new ChromeDriver();
		
		driver.get("http://www.softwaretestingstuff.com/");
		
		String StringName = driver.findElement(By.linkText("ETL Testing Interview Questions")).getText();
		System.out.println(StringName);
	}

}
